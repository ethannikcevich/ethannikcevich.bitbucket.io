var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "bcdemqst",
  2: "n",
  3: "bcdefhlmst",
  4: "_cdefghmnoprstuwz",
  5: "acefiklnoprstvwxyz",
  6: "ehlp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

