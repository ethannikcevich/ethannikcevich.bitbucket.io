var searchData=
[
  ['sawwave_0',['SawWave',['../_lab0x01_8py.html#ad2c4f9d2a07b321f037d772e899b9ccb',1,'Lab0x01']]],
  ['scalar_1',['scalar',['../classtouchpad_1_1_touchpad.html#a657249b2a0a22232da345cae4a248f01',1,'touchpad::Touchpad']]],
  ['scale_5flength_2',['scale_length',['../classtouchpad_1_1_touchpad.html#a7d3581bd29e49e11205429931ee538cc',1,'touchpad::Touchpad']]],
  ['scale_5fwidth_3',['scale_width',['../classtouchpad_1_1_touchpad.html#a4051d7a73aee5b5e470ce7de8e6b7d78',1,'touchpad::Touchpad']]],
  ['set_5fcalibration_5fcoefficients_4',['set_calibration_coefficients',['../class_b_n_o055_1_1_b_n_o055.html#a7fe46399783a6d044f3005b55dafd78c',1,'BNO055.BNO055.set_calibration_coefficients(self, setbuf)'],['../class_b_n_o055_1_1_b_n_o055.html#a7fe46399783a6d044f3005b55dafd78c',1,'BNO055.BNO055.set_calibration_coefficients(self, setbuf)']]],
  ['set_5fduty_5',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty()'],['../classdriver_1_1_motor.html#a9f2eb4b398e2ef56812306540fe03301',1,'driver.Motor.set_duty()'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)']]],
  ['set_5fgain_6',['set_Gain',['../class_closed_loop_1_1_closed_loop.html#af76cc3fbca8ada5aeb769ca91d3c0298',1,'ClosedLoop.ClosedLoop.set_Gain(self, Kp, Kd, Ki)'],['../class_closed_loop_1_1_closed_loop.html#af141c69240a2c1618fa52ec580fd023b',1,'ClosedLoop.ClosedLoop.set_Gain(self, Kp, Ki)'],['../class_closed_loop_1_1_closed_loop.html#af76cc3fbca8ada5aeb769ca91d3c0298',1,'ClosedLoop.ClosedLoop.set_Gain(self, Kp, Kd, Ki)']]],
  ['set_5fomega_5fref_7',['set_Omega_ref',['../class_closed_loop_1_1_closed_loop.html#a4921295a9645129f8467cf6d8940576e',1,'ClosedLoop.ClosedLoop.set_Omega_ref(self, Omega_ref)'],['../class_closed_loop_1_1_closed_loop.html#a4921295a9645129f8467cf6d8940576e',1,'ClosedLoop.ClosedLoop.set_Omega_ref(self, Omega_ref)']]],
  ['set_5fref_8',['set_ref',['../class_closed_loop_1_1_closed_loop.html#a34a67e7f9b89f55a85a41d751b22ddfe',1,'ClosedLoop::ClosedLoop']]],
  ['share_9',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_10',['shares.py',['../_final_project_2shares_8py.html',1,'(Global Namespace)'],['../_lab2_2shares_8py.html',1,'(Global Namespace)'],['../_lab3_2shares_8py.html',1,'(Global Namespace)'],['../_lab4_2shares_8py.html',1,'(Global Namespace)'],['../_lab5_2shares_8py.html',1,'(Global Namespace)']]],
  ['sinewave_11',['SineWave',['../_lab0x01_8py.html#a2a453bdf439ecdb8315cf67b7c45a67d',1,'Lab0x01']]],
  ['size_12',['size',['../classtouchpad_1_1_touchpad.html#afbc5934232a056c39ac7e564c6ccd06e',1,'touchpad::Touchpad']]],
  ['squarewave_13',['SquareWave',['../_lab0x01_8py.html#ae3c6400153b6640b4e18d70551374e94',1,'Lab0x01']]]
];
