var searchData=
[
  ['cal_5fbyte_0',['cal_byte',['../class_b_n_o055_1_1_b_n_o055.html#a15786f5a63db81ff89cadfcd3ceb34b1',1,'BNO055::BNO055']]],
  ['calibrate_1',['calibrate',['../classtouchpad_1_1_touchpad.html#a57248241edac0dfc23af5cf1f249555d',1,'touchpad::Touchpad']]],
  ['center_5flength_2',['center_length',['../classtouchpad_1_1_touchpad.html#a687a42b79e5c4b6d542fe943f58e82d4',1,'touchpad::Touchpad']]],
  ['center_5fwidth_3',['center_width',['../classtouchpad_1_1_touchpad.html#aa12ab6ae86592cbd9ccd44045a517030',1,'touchpad::Touchpad']]],
  ['change_5fmode_4',['change_mode',['../class_b_n_o055_1_1_b_n_o055.html#a46879e92a286997c935930164e6d850d',1,'BNO055.BNO055.change_mode(self, mode)'],['../class_b_n_o055_1_1_b_n_o055.html#a46879e92a286997c935930164e6d850d',1,'BNO055.BNO055.change_mode(self, mode)']]],
  ['closedloop_5',['ClosedLoop',['../class_closed_loop_1_1_closed_loop.html',1,'ClosedLoop']]],
  ['closedloop_2epy_6',['ClosedLoop.py',['../_final_project_2_closed_loop_8py.html',1,'(Global Namespace)'],['../_lab4_2_closed_loop_8py.html',1,'(Global Namespace)'],['../_lab5_2_closed_loop_8py.html',1,'(Global Namespace)']]]
];
