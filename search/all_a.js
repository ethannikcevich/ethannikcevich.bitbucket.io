var searchData=
[
  ['k_5fxx_0',['K_xx',['../classtouchpad_1_1_touchpad.html#aa378a12982730817d15836c0e715e13c',1,'touchpad::Touchpad']]],
  ['k_5fxy_1',['K_xy',['../classtouchpad_1_1_touchpad.html#af22f3c6416db0f34f85b111b4467fedb',1,'touchpad::Touchpad']]],
  ['k_5fyx_2',['K_yx',['../classtouchpad_1_1_touchpad.html#a7cd105fd452a4a7869751c1d75d6bc79',1,'touchpad::Touchpad']]],
  ['k_5fyy_3',['K_yy',['../classtouchpad_1_1_touchpad.html#a782a89416ab92fb01816126745e3696b',1,'touchpad::Touchpad']]],
  ['kd_4',['Kd',['../class_closed_loop_1_1_closed_loop.html#abd4cbb06cb8ba55f6aec2718cedf3a95',1,'ClosedLoop::ClosedLoop']]],
  ['ki_5',['Ki',['../class_closed_loop_1_1_closed_loop.html#af5c07eb738bea6f8f038dcc7a46024e7',1,'ClosedLoop::ClosedLoop']]],
  ['kp_6',['Kp',['../class_closed_loop_1_1_closed_loop.html#a600c2ee167190f2afdf9fc38c849cf01',1,'ClosedLoop::ClosedLoop']]]
];
